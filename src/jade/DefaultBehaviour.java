/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade;

import jade.core.behaviours.Behaviour;
import java.io.IOException;

/**
 *
 * @author mike
 */
public class DefaultBehaviour extends Behaviour {
    @Override
    public void action(){
        
    }

    @Override
    public boolean done() {
        System.out.println("Behaviour done\n");
        return true;
    }
    
    public void Execute(String command) {
        try {
	Runtime.getRuntime().exec(command); 
        }
        catch (IOException exception) {
            System.out.println (exception);
        }
    }
        
    public void Execute(String command, String[] param){
        try {
                String [] cmd = {command,param[1],param[2], param[3]}; //Unix's command and parameters
                Runtime.getRuntime().exec(cmd);
        } catch (IOException exception) {
                System.out.println (exception);
        }
    }    
    
    public int IdentifyOS(String command){
        int correct=0;
        try{
            Execute(command);
            //Execute("uname -o");    //Execute("ver");   //Execute("sw_vers");
        }
        catch(Exception e){
            correct=1; //it can't be done
        }
        return correct;
    }
        
    public void Check() {
        String [] OS = {"uname -o", "ver", "sw_ver"};
        int Linux=0; Linux= IdentifyOS(OS[0]);
        int Win=0; Win= IdentifyOS(OS[1]);
        int Mac=0; Mac= IdentifyOS(OS[2]);
        if((Linux |Mac) !=0 | Win==0) System.out.println("OS is Windows");
        if((Win | Linux) !=0 | Mac==0) System.out.println("OS is Mac");
        if((Mac | Win) !=0 | Linux==0) System.out.println("OS is Linux");
    }
    
    public void NetworkTest(){      
        String [] testPC = {"127.0.0.1"};
        String [] net = {"www.google.com"};
        Execute("Ping",testPC); //router test
        Execute("Ping",net); //internet test                    
    }
    
    public void CheckCyberMatter(){
        Execute("nmap -p 444-445 localhost"); //check port 445 (currently news)
    }
    
}
