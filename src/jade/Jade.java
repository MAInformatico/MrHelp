/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jade;

import jade.core.Agent;
import java.io.IOException;

/**
 *
 * @author mike
 */
public class Jade extends Agent {

    /**
     * Main class
     * @version 1.0
     * @param args the command line arguments
     */
    
    private String status;
       
    protected void setup(){
        System.out.println("Hi, I'm agent " + getAID().getName() + "\n");
        System.out.println("In there to help you in maintain tasks\n");
    }
    
    protected void takeDown(){
        System.out.println("I've done my job\n");
    }
    
    
    /*
    public static void main(String[] args) {
        // TODO code application logic here
    }
    */
}
